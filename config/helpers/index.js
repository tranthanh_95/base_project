/**
 * This file export many useful function
 */
"use strict";

const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const url = require('url');

module.exports = {
  /**
   * Return a random number
   * @param length - number of characters - default 6 - max 15
   * @returns {string}
   */
  randomNumber: (length = 6) => {
    if (length > 15) length = 15;
    let result = '';
    for (let i = 0; i < length; i++) {
      let randomNumber = Math.floor(Math.random() * 10);
      result += String(randomNumber);
    }
    return result;
  },


};
