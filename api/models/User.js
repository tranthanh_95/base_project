"use strict";

const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    uid                       : {type: Number, unique: true},
    email                     : { type: String, required: true},
    password                  : {type: String, required: true},
}, { timestamps: true });

UserSchema.pre('save', async function save (next) {
    try {
        const user = this;

        let lastUserUid = await User.count(),
            startUserId = 10001;
        user.uid = lastUserUid + startUserId;

        // hash password
        if (user.password)
            user.password = await BcryptService.hash(user.password);

        next();
    } catch (error) {
        next(error)
    }
});

module.exports = mongoose.model('User', UserSchema);
