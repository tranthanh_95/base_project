"use strict";

/** Admin Route */
let adminRoutes = {
  'get  /': 'UserController.index',
};

// Route faker data.
let fakerRoutes = {};
if (process.env.NODE_ENV !== 'production') {
  fakerRoutes = {

  }
}

let routes = Object.assign({}, adminRoutes, fakerRoutes);

module.exports.routes = routes;
