"use strict";

module.exports = {

  /**
   *  dashboard admin
   */
  index: asyncWrap(async (req, res) => {

    res.render('homepage');
  }),

};
