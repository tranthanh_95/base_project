"use strict";

/**
 * Event Listener: Handle Email Event
 * [{
 *    eventName: 'model.action',
 *    handler: function(data){}
 * }]
 */

module.exports = [
  {
    eventName: 'send-verify-email',
    handler: async() => {
      try {

      } catch (error) {
        sails.log.error('EmailEvent - user.register: ', error);
      }
    },
  },

];
